import os

#tuple to define names of the days for the week

days_name = ("monday", "tuesday", "wednesday", "thursday", "friday", "staurday", "sunday")

week_plans = []

#function to load file

def load_plans(file_name):

     if os.path.exists(file_name):

         with open(file_name, 'r') as file:

             lines = file.readlines()

             for line in lines:

                 line = line.strip()

                 week_plans = line.split(" ")

     return week_plans

     print("Plans are loaded successfully")

#function to store plans for the day

def store_plans(day, start_time, end_time, day_plan):

    week_plans.append((day, start_time, end_time, day_plan))

    print("Plan is now added")

#function to show plans for the day
def show_plans():

    day = input("Which day? ").lower()
    if day not in days_name:
        print("Ooops! There is no day with this name.")
    
    # Check if there are any plans for the specified day
    plans_for_day = [plan for plan in week_plans if day == plan[3].lower()]

    if not plans_for_day:

        print(f"You have no plans for {day.lower()}.")

    else:

        print(f'Your plans for {day.lower()} are:')

        for plan in plans_for_day:

            print(f"Start Time: {plan[0]}")

            print(f"End Time: {plan[1]}")

            print(f"Day Plan: {plan[2]}")

def save_plans(file_name, week_plans):

    f = input("File name? ")

    with open(file_name, 'w') as file:

        for plan in week_plans:

            file.write(' '.join(plan) + '\n')

    print("Your calender is saved.")

# Main loop

while True:

    file_name = "week_plans.txt"

    print("Choose: load, add, show, save, exit")

    choice = input("choose from the list: ")

    if choice == "load":

        week_plans = load_plans(file_name)

        print("Plans are now loaded.")

    elif choice == "add":

        day = input("Which day? ").lower()

        if day not in days_name:

            print("Ooops! There is no day with this name.")

        start_time = input("Start time?  ")

        end_time = input("End time? ")

        day_plan = input("What is the plan?  ")

        store_plans(day, start_time, end_time, day_plan)

    elif choice == "show":

        show_plans()

    elif choice == "save":

        save_plans(file_name, week_plans)

    elif choice == "exit":

        print("Thank you, have a nice day")

        break

    else:

        print("Wrong choice. Please try again.")